﻿using WeDonekRpc.TcpServer.FileUp.Model;
using System.IO;

namespace WeDonekRpc.Client.FileUp.Model
{
    internal class SocketUpResult : IUpResult
    {
        private UpFileResult _result;
        public SocketUpResult(UpFileResult result)
        {
            _result = result;
        }

        /// <summary>
        /// 文件MD5
        /// </summary>
        public string FileMd5 => _result.FileMd5;

        public void Save(string savePath)
        {
            this._result.Save(savePath);
        }
        public byte[] GetByte()
        {
            return this._result.GetByte();
        }

        public Stream GetStream()
        {
            return this._result.GetStream();
        }
    }
}

﻿using System;
using System.IO;

namespace WeDonekRpc.Helper.Http
{
    public static class LinqHelper
    {
        public static string GetExtension(this HttpStreamRes res,Uri uri)
        {
            string extension=Path.GetExtension(uri.LocalPath);
            if (extension.IsNotNull())
            {
                return extension;
            }
            else if (!res.ContentType.IsNull())
            {
                return HttpHeaderHelper.GetExtension(res.ContentType);
            }
            return null;
        }
    }
}

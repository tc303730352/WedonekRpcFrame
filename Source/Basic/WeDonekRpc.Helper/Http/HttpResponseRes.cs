﻿using System.Net;
using System.Text;

namespace WeDonekRpc.Helper
{
    /// <summary>
    /// 请求结果
    /// </summary>
    public class HttpResponseRes
    {
        private byte[] _data;
        private Encoding _encoding;
        internal HttpResponseRes(byte[] datas,
            KeValue[] cookies,
        Encoding encoding,
            string contentType,
            HttpStatusCode status)
        {
            this.ContentType = contentType;
            this.Cookies = cookies;
            this.StatusCode = status;
            this._data = datas;
            this._encoding = encoding;
        }
        /// <summary>
        /// 状态码
        /// </summary>
        public HttpStatusCode StatusCode
        {
            get;
        }
        /// <summary>
        /// 响应头类型
        /// </summary>
        public string ContentType
        {
            get;
        }
        /// <summary>
        /// 源数据流
        /// </summary>
        public byte[] SourceBytes
        {
            get => _data;
        }
        private string _Content;
        /// <summary>
        /// 返回的内容
        /// </summary>
        public string Content
        {
            get
            {
                if (this._Content == null)
                {
                    this._Content = _encoding.GetString(_data).Replace("\0", string.Empty);
                }
                return this._Content;
            }
        }
        /// <summary>
        /// 返回的Cookie
        /// </summary>
        public KeValue[] Cookies { get; }
    }
    public struct KeValue
    {
        public string Name
        {
            get;
            set;
        }
        public string Value
        {
            get;
            set;
        }
    }
}

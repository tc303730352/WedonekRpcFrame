﻿using System;
using System.Net;
using WeDonekRpc.Helper.Json;

namespace WeDonekRpc.Helper.Http
{
    public enum HttpReqType
    {
        basic = 0,
        Json = 1,
        image = 2,
        File = 3,
        Html = 4,
        XML = 5
    }
    /// <summary>
    /// HTTP请求帮助
    /// </summary>
    public class HttpHelper
    {
        private static readonly HttpRequestSet _DownImgMethodSet = new HttpRequestSet("GET", HttpReqType.image)
        {
            Timeout = 60000,
            ReadWriteTimeout = 4000,
            ContinueTimeout = 20000,
            Referer = "http://mp.weixin.qq.com/",
            IsResetFileName = true
        };
        private static readonly HttpRequestSet _DownFileMethodSet = new HttpRequestSet(HttpReqType.File)
        {
            Timeout = 130000,
            ReadWriteTimeout = 120000,
            ContinueTimeout = 10000
        };
        private const string _PostMethod = "POST";
        public static readonly HttpRequestSet GetMethodSet = new HttpRequestSet(HttpReqType.basic);
        public static readonly HttpRequestSet GetMethodJson = new HttpRequestSet(HttpReqType.Json);
        public static readonly HttpRequestSet PostMethodSet = new HttpRequestSet(_PostMethod, HttpReqType.basic);
        public static readonly HttpRequestSet PostJsonMethodSet = new HttpRequestSet(_PostMethod, HttpReqType.Json);
        private static readonly HttpRequestSet _UpFileMethodSet = new HttpRequestSet(HttpReqType.File)
        {
            Timeout = 120000,
            ReadWriteTimeout = 120000,
            ContinueTimeout = 120000
        };
        static HttpHelper ()
        {
            GetMethodSet = new HttpRequestSet(HttpReqType.basic);
        }

        #region 下载图片文件
        /// <summary>
        /// 下载图片文件
        /// </summary>
        /// <param name="uri">图片地址</param>
        /// <param name="savePath">保存路径</param>
        /// <returns>Http状态码</returns>
        public static HttpStatusCode DownImageFile (Uri uri, ref string savePath)
        {
            return HttpTools.DownImageFile(uri, ref savePath, HttpHelper._DownImgMethodSet);
        }
        /// <summary>
        /// 下载图片文件
        /// </summary>
        /// <param name="uri">图片地址</param>
        /// <param name="savePath">保存路径</param>
        /// <param name="reqSet">请求设置</param>
        /// <returns>Http状态码</returns>
        public static HttpStatusCode DownImageFile (Uri uri, ref string savePath, HttpRequestSet reqSet)
        {
            return HttpTools.DownImageFile(uri, ref savePath, reqSet);
        }
        #endregion


        /// <summary>
        /// 发送Http请求
        /// </summary>
        /// <param name="uri">请求地址</param>
        /// <param name="post">Post参数</param>
        /// <param name="config">请求配置</param>
        /// <returns>请求结果</returns>
        public static HttpResponseRes SendRequest (Uri uri, string post, HttpRequestSet config)
        {
            return HttpTools.SendRequest(uri, post, config);
        }
        #region 提交GET请求

        public static HttpResponseRes HttpGet (Uri uri, HttpRequestSet config)
        {
            return HttpTools.SendRequest(uri, null, config);
        }
        public static HttpResponseRes HttpGet (Uri uri)
        {
            return HttpTools.SendRequest(uri, null, HttpHelper.GetMethodSet);
        }
        public static string Get (Uri uri)
        {
            return HttpTools.HttpGet(uri, HttpHelper.GetMethodSet);
        }
        public static T Get<T> (Uri uri)
        {
            string json = HttpTools.HttpGet(uri, HttpHelper.GetMethodJson);
            return JsonTools.Json<T>(json);
        }
        public static T Get<T> (Uri uri, HttpRequestSet config)
        {
            config.SubmitMethod = "GET";
            string json = HttpTools.HttpGet(uri, config);
            return JsonTools.Json<T>(json);
        }
        #endregion


        #region 提交POST请求
        public static string Post (Uri uri, string post, HttpRequestSet reqSet)
        {
            reqSet.SubmitMethod = _PostMethod;
            return HttpTools.HttpPost(uri, post, reqSet);
        }
        public static HttpResponseRes HttpPost (Uri uri, string post)
        {
            return HttpTools.SendRequest(uri, post, HttpHelper.PostMethodSet);
        }
        public static HttpResponseRes HttpPost (Uri uri, string post, HttpRequestSet reqSet)
        {
            reqSet.SubmitMethod = _PostMethod;
            return HttpTools.SendRequest(uri, post, reqSet);
        }
        public static string PostForm (Uri uri, string post)
        {
            return HttpTools.HttpPost(uri, post, HttpHelper.PostMethodSet);
        }
        public static T PostForm<T> (Uri uri, string post)
        {
            string json = HttpTools.HttpPost(uri, post, HttpHelper.PostMethodSet);
            return JsonTools.Json<T>(json);
        }
        public static T HttpPost<T> (Uri uri, string post, HttpRequestSet reqSet)
        {
            reqSet.SubmitMethod = _PostMethod;
            string json = HttpTools.HttpPost(uri, post, reqSet);
            return JsonTools.Json<T>(json);
        }
        #endregion



        #region 提交JSON数据
        public static string PostJson<T> (Uri uri, T data)
        {
            string json = JsonTools.Json(data);
            return HttpTools.HttpPost(uri, json, HttpHelper.PostJsonMethodSet);
        }
        public static T PostJson<T> (Uri uri, string json)
        {
            string str = HttpTools.HttpPost(uri, json, HttpHelper.PostJsonMethodSet);
            return JsonTools.Json<T>(str);
        }

        public static Result PostJson<T, Result> (Uri uri, T data)
        {
            return PostJson<Result>(uri, JsonTools.Json(data));
        }
        public static string PostJson (Uri uri, string json)
        {
            return HttpTools.HttpPost(uri, json, HttpHelper.PostJsonMethodSet);
        }
        public static HttpStatusCode PostJsonVoid (Uri uri, string json)
        {
            return HttpTools.HttpPostVoid(uri, json, HttpHelper.PostJsonMethodSet);
        }

        #endregion


        #region 下载文件

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="post"></param>
        /// <param name="savePath"></param>
        /// <returns></returns>
        public static HttpStatusCode DownFile (Uri uri, string post, ref string savePath)
        {
            return HttpTools.DownFile(uri, post, ref savePath, _DownFileMethodSet);
        }
        public static HttpStatusCode DownFile (Uri uri, string post, ReadStream read)
        {
            return HttpTools.DownFile(uri, post, read, _DownFileMethodSet);
        }
        public static HttpStatusCode DownFile (Uri uri, string post, ref string savePath, HttpRequestSet reqSet)
        {
            return HttpTools.DownFile(uri, post, ref savePath, reqSet);
        }
        public static HttpStatusCode DownFile (Uri uri, string post, ReadStream read, HttpRequestSet reqSet)
        {
            return HttpTools.DownFile(uri, post, read, reqSet);
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="savePath"></param>
        /// <param name="reqSet"></param>
        /// <returns></returns>
        public static HttpStatusCode DownFile (Uri uri, ref string savePath)
        {
            return HttpTools.DownFile(uri, null, ref savePath, _DownFileMethodSet);
        }
        public static HttpStatusCode DownFile (Uri uri, ref string savePath, HttpRequestSet reqSet)
        {
            return HttpTools.DownFile(uri, null, ref savePath, reqSet);
        }
        #endregion
        #region 上传文件
        public static string UploadFile (Uri uri, string post, string filePath)
        {
            byte[] bytes = HttpTools.UploadFile(uri, post, filePath, HttpHelper._UpFileMethodSet);
            return HttpHelper._UpFileMethodSet.ResponseEncoding.GetString(bytes).Replace("\0", string.Empty);
        }

        public static T UploadFile<T> (Uri uri, string post, string filePath)
        {
            string json = UploadFile(uri, post, filePath);
            return JsonTools.Json<T>(json);
        }
        #endregion
        #region 获取远程数据流
        public static byte[] GetStream (Uri uri, string post)
        {
            byte[] datas = HttpTools.HttpPostRequest(uri, post, HttpHelper.PostMethodSet, out HttpStatusCode code);
            if (code != HttpStatusCode.OK)
            {
                return datas;
            }
            return datas;
        }
        public static byte[] GetStream (Uri uri, string post, HttpRequestSet reqSet)
        {
            reqSet.SubmitMethod = _PostMethod;
            return HttpTools.HttpPostRequest(uri, post, reqSet);
        }
        public static HttpStreamRes GetResponseStream (Uri uri, string post)
        {
            return HttpTools.GetStream(uri, post, HttpHelper.PostMethodSet);
        }
        public static HttpStreamRes GetResponseStream (Uri uri, string post, HttpRequestSet reqSet)
        {
            reqSet.SubmitMethod = _PostMethod;
            return HttpTools.GetStream(uri, post, reqSet);
        }
        public static HttpStreamRes GetResponseStream (Uri uri)
        {
            return HttpTools.GetStream(uri, null, HttpHelper.GetMethodSet);
        }
        public static byte[] GetStream (Uri uri)
        {
            return HttpTools.HttpPostRequest(uri, null, HttpHelper.GetMethodSet);
        }
        public static HttpStreamRes GetStream (Uri uri, HttpRequestSet set)
        {
            set.SubmitMethod = "GET";
            return HttpTools.GetStream(uri, null, set);
        }

        #endregion

    }
}

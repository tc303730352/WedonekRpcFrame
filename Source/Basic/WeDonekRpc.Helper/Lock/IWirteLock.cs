﻿namespace WeDonekRpc.Helper
{
        public interface IReadWirteLock : System.IDisposable
        {
                bool GetLock();
        }
}
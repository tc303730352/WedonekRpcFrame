﻿using System;
using System.Threading;

namespace WeDonekRpc.Helper.Lock
{
    public class DataSyncLock : IDisposable
    {
        private int _UseNum = 0;
        private int _Time = int.MaxValue;
        private readonly SyncLock _Lock = new SyncLock();
        private volatile bool _IsSync = false;
        public string Key
        {
            get;
        }
        internal DataSyncLock(string key)
        {
            this.Key = key;
        }
        public object Result
        {
            get;
            private set;
        }

        public bool GetLock()
        {
            if (_Lock.GetLock())
            {
                this._Time = HeartbeatTimeHelper.HeartbeatTime;
                this._IsSync = true;
                Interlocked.Add(ref this._UseNum, 1);
                return true;
            }
            return false;
        }
        internal bool IsNoUse(int time)
        {
            return Interlocked.CompareExchange(ref _UseNum, 0, 0) == 0 && _Time < time;
        }
     
        public void Exit(object result)
        {
            if (this._IsSync)
            {
                this.Result = result;
                this._IsSync = false;
                Interlocked.Add(ref this._UseNum, -1);
                this._Lock.Exit();
            }
        }
        public void Dispose()
        {
            if (this._IsSync)
            {
                Interlocked.Add(ref this._UseNum, -1);
                this._IsSync = false;
                this._Lock.Reset();
            }
        }
    }
}

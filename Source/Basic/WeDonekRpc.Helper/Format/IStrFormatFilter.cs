﻿namespace WeDonekRpc.Helper
{
        public interface IStrFormatFilter
        {
                string FormatStr(object source, string str);
        }
}
﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace WeDonekRpc.Helper
{
    public class ImageTools
    {
        private static Size _GetResizeSizeByHeight(Image mat, int height)
        {
            int width = (int)Math.Round(mat.Width * (height * 1.0 / mat.Height));
            return new Size(width, height);
        }
        private static Size _GetResizeSizeByWidth(Image mat, int width)
        {
            int height = (int)Math.Round(mat.Height * (width * 1.0 / mat.Width));
            return new Size(width, height);
        }
        private static Size _GetSize(Image original, int? width, int? height)
        {
            if (width.HasValue && height.HasValue)
            {
                if (width.Value >= height.Value)
                {
                    return _GetResizeSizeByHeight(original, height.Value);
                }
                else
                {
                    return _GetResizeSizeByWidth(original, width.Value);
                }
            }
            else if (width.HasValue)
            {
                return _GetResizeSizeByWidth(original, width.Value);
            }
            return _GetResizeSizeByHeight(original, height.Value);
        }
        public static byte[] Resize(Image original, int? width, int? height)
        {
            Size size = _GetSize(original, width, height);
            using (Image img = _Resize(original, size))
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    img.Save(stream, original.RawFormat);
                    stream.Position = 0;
                    return stream.ToArray();
                }
            }
        }
        public static byte[] Resize(FileInfo file, int? width, int? height)
        {
            using (Image img = Image.FromFile(file.FullName))
            {
                return Resize(img, width, height);
            }
        }

        private static Image _Resize(Image original, Size size)
        {
            if (original.Width == size.Width && original.Height == size.Height)
            {
                return original;
            }
            Bitmap bitmap = new Bitmap(size.Width, size.Height);
            //新建一个画板  
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                //设置高质量插值法  
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                //设置高质量,低速度呈现平滑程度  
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //清空画布并以透明背景色填充  
                g.Clear(Color.Transparent);
                //在指定位置并且按指定大小绘制原图片的指定部分  
                g.DrawImage(original, new Rectangle(0, 0, size.Width, size.Height), new Rectangle(0, 0, original.Width, original.Height), GraphicsUnit.Pixel);
                g.Save();
            }
            return bitmap;
        }
    }
}

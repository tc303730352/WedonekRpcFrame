﻿namespace WeDonekRpc.Helper
{
    public interface IAutoTask
    {
        bool IsExec (int now);
        void ExecuteTask ();
    }
}
﻿using WeDonekRpc.Helper;

namespace WeDonekRpc.CacheClient.Interface
{
    public interface ISubscriberController : IDataSyncClass
    {
        string SubName { get; }
    }
}
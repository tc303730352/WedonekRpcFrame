﻿using System;
using CSRedis;
using WeDonekRpc.CacheClient.Cache;
using WeDonekRpc.CacheClient.Interface;
using WeDonekRpc.CacheClient.Redis;

namespace WeDonekRpc.CacheClient.RedisCache
{
    internal class RedisController : CacheController, IRedisController
    {
        protected readonly CSRedisClient _Client;
        public RedisController () : base(Interface.CacheType.Redis)
        {
            this._Client = RedisCommon.RedisClient;
        }
        public bool Exists (string key)
        {
            key = RpcCacheService.FormatKey(key);
            return this._Client.Exists(key);
        }
        public bool SetExpire (string key, TimeSpan time)
        {
            key = RpcCacheService.FormatKey(key);
            return this._Client.Expire(key, time);
        }
    }
}

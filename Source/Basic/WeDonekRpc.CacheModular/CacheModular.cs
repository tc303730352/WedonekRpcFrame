﻿using WeDonekRpc.CacheClient;
using WeDonekRpc.CacheModular;
using WeDonekRpc.Client;
using WeDonekRpc.Client.Interface;

namespace RpcCacheModular
{
    public class CacheModular : IRpcInitModular
    {
        public void Init (IIocService ioc)
        {

        }
        public void Load (RpcInitOption option)
        {
            RpcCacheService.Load(new CacheIocContainer(option));
        }
        public void InitEnd (IIocService service)
        {
        }

    }
}
﻿using WeDonekRpc.CacheClient.Config;
using WeDonekRpc.CacheClient.Interface;

namespace RpcCacheModular
{
    public interface IRpcCacheConfig
    {
        CacheType CacheType { get; }
        CacheConfig Cache { get; }
    }
}
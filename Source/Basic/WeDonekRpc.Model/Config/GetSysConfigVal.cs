﻿namespace WeDonekRpc.Model
{
        [IRemoteConfig("GetSysConfigVal", "sys.sync")]
        public class GetSysConfigVal
        {
                public string Name
                {
                        get;
                        set;
                }
        }
}

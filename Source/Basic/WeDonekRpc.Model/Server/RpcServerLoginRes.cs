﻿namespace WeDonekRpc.Model
{
    public class RpcServerLoginRes
    {
        public RpcConfig OAuthConfig
        {
            get;
            set;
        }

        public RpcServerConfig ServerConfig
        {
            get;
            set;
        }
    }
}

﻿using WeDonekRpc.Client;
using WeDonekRpc.Modular;

namespace RpcExtend.Service
{
    public class ExtendService
    {
        public static void InitService ()
        {
            RpcClient.RpcEvent = new RpcExtendEvent();
            RpcClient.Start();
        }
        private class RpcExtendEvent : RpcEvent
        {
            public override void Load (RpcInitOption option)
            {
                option.LoadModular<ExtendModular>();
                option.Load("RpcExtend.Service");
            }
        }
        public static void CloseService ()
        {
            RpcClient.Close();
        }
    }
}

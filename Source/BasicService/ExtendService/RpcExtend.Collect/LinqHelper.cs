﻿using System.Collections.Generic;

namespace RpcExtend.Collect
{
    internal static class LinqHelper
    {
        public static long GetLong(this Dictionary<string, string> dic, string key, long def)
        {
            if (dic.TryGetValue(key, out string val))
            {
                return long.Parse(val);
            }
            return def;
        }
    }
}

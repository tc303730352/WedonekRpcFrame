﻿using RpcSync.Model;

namespace RpcSync.DAL
{
    public interface IRemoteServerGroupDAL
    {
        MerServer[] GetAllServer (long merId);
        long[] GetHoldServerId (long merId);
    }
}
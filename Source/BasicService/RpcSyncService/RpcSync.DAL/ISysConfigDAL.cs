﻿using RpcSync.Model;
using RpcSync.Model.DB;

namespace RpcSync.DAL
{
    public interface ISysConfigDAL
    {
        SysConfig[] GetSysConfig (string type);
        ConfigItemToUpdateTime[] GetToUpdateTime ();
    }
}
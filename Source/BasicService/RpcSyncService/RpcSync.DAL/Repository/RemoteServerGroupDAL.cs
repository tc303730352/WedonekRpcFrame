﻿using RpcSync.Model;
using RpcSync.Model.DB;
using WeDonekRpc.SqlSugar;

namespace RpcSync.DAL.Repository
{
    internal class RemoteServerGroupDAL : IRemoteServerGroupDAL
    {
        private readonly IRepository<RemoteServerGroupModel> _BasicDAL;
        public RemoteServerGroupDAL (IRepository<RemoteServerGroupModel> dal)
        {
            this._BasicDAL = dal;
        }
        public MerServer[] GetAllServer (long merId)
        {
            return this._BasicDAL.Gets<MerServer>(c => c.RpcMerId == merId);
        }

        public long[] GetHoldServerId (long merId)
        {
            return this._BasicDAL.Gets(a => a.RpcMerId == merId && a.IsHold, a => a.ServerId);
        }
    }
}

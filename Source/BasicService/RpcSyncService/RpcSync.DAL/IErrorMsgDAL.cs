﻿using RpcSyncService.Model.DAL_Model;

namespace RpcSync.DAL
{
    public interface IErrorMsgDAL
    {
        string GetErrorMsg(long errorId,string lang);
    }
}
﻿using RpcSync.Service.Interface;
using WeDonekRpc.CacheClient.Interface;
using WeDonekRpc.Client.Attr;
using WeDonekRpc.Helper;
using WeDonekRpc.Model;
using WeDonekRpc.ModularModel.Accredit;
using WeDonekRpc.ModularModel.Accredit.Model;

namespace RpcSync.Service.Accredit
{
    [IocName("redis")]
    [ClassLifetimeAttr(ClassLifetimeType.SingleInstance)]
    internal class RedisAccreditToken : IAccreditToken
    {
        private AccreditToken _Token;

        private string _CacheKey;
        private const string _CacheName = "Accredit_";

        private readonly IRedisListController _RedisList;
        private readonly IRedisController _Cache;
        private readonly IClearAccreditQueue _ClearQueue;

        public RedisAccreditToken (IRedisListController redisList,
            IClearAccreditQueue clear,
            IRedisController redis)
        {
            this._ClearQueue = clear;
            this._RedisList = redisList;
            this._Cache = redis;
        }

        public string AccreditId
        {
            get;
            private set;
        }
        public string CheckKey => this._Token.CheckKey;

        public AccreditToken Token => this._Token;

        private void _InitCache ()
        {
            this._CacheKey = _CacheName + this.AccreditId;
        }
        public void Create (AccreditToken token)
        {
            this.AccreditId = token.AccreditId;
            this._Token = token;
            this._InitCache();
            TimeSpan? time = token.GetOverTime();
            if (!time.HasValue)
            {
                throw new ErrorException("accredit.expire");
            }
            else if (!this._Cache.Add(this._CacheKey, token, time.Value))
            {
                throw new ErrorException("accredit.add.error");
            }
            else if (token.PAccreditId.IsNotNull())
            {
                string subKey = "AccreditSub_" + token.PAccreditId;
                try
                {
                    _ = this._RedisList.Append(subKey, this.AccreditId);
                }
                catch (Exception e)
                {
                    _ = this._Cache.Remove(this._CacheKey);
                    throw ErrorException.FormatError(e);
                }
            }
        }
        public AccreditDatum Get ()
        {
            return new AccreditDatum
            {
                RoleType = this._Token.RoleType,
                StateVer = this._Token.StateVer,
                State = this._Token.State,
                Accredit = new AccreditRes
                {
                    AccreditId = this._Token.AccreditId,
                    CheckKey = this._Token.CheckKey,
                    SysGroup = this._Token.SysGroup,
                    ApplyId = this._Token.ApplyId,
                    RpcMerId = this._Token.RpcMerId,
                    Expire = this._Token.Expire
                }
            };
        }
        public void Set (SetAccredit obj)
        {
            this._Token.AccreditRole = obj.AccreditRole ?? Array.Empty<string>();
            this._Token.State = obj.State;
            this._Token.Expire = !obj.Expire.HasValue ? null : DateTime.Now.AddSeconds(obj.Expire.Value);
            this._Token.StateVer += 1;
            _ = this._SaveToken();
        }
        public TimeSpan? Refresh ()
        {
            if (this._Token.Expire.HasValue)
            {
                return null;
            }
            TimeSpan time = AccreditHelper.GetDefAccreditTime();
            if (this._Cache.SetExpire(this._CacheKey, time))
            {
                return time;
            }
            return null;
        }


        private bool _SaveToken ()
        {
            TimeSpan? time = this._Token.GetOverTime();
            if (!time.HasValue)
            {
                throw new ErrorException("accredit.Invalid");
            }
            AccreditToken newToken = this._Cache.AddOrUpdate<AccreditToken>(this._CacheKey, this._Token, (a, b) =>
            {
                if (a.StateVer < b.StateVer)
                {
                    return b;
                }
                return null;
            }, time.Value);
            if (newToken == null)
            {
                throw new ErrorException("accredit.set.fail");
            }
            else if (!newToken.Equals(this._Token))
            {
                this._Token = newToken;
                this._Token.Refresh(true);
                return true;
            }
            return false;
        }
        public bool SetState (string state)
        {
            this._Token.State = state;
            this._Token.StateVer += 1;
            return this._SaveToken();
        }
        public bool Cancel ()
        {
            if (this.Remove(out string[] keys))
            {
                this._Token.Refresh(true);
                if (!keys.IsNull())
                {
                    this._ClearQueue.Add(keys);
                }
                return true;
            }
            return false;
        }
        public bool Remove (out string[] subs)
        {
            if (!this._Cache.Remove(this._CacheKey))
            {
                subs = null;
                return false;
            }
            if (this._Token.PAccreditId.IsNotNull())
            {
                string key = "AccreditSub_" + this._Token.PAccreditId;
                subs = this._RedisList.Gets<string>(key);
                return this._Cache.Remove(key);
            }
            else
            {
                subs = null;
            }
            return true;
        }
        public bool Init (string accreditId)
        {
            this.AccreditId = accreditId;
            this._InitCache();
            if (this._Cache.TryGet(this._CacheKey, out AccreditToken token))
            {
                this._Token = token;
                if (token.Expire.HasValue)
                {
                    return token.Expire.Value > DateTime.Now;
                }
                return true;
            }
            return false;
        }



        public bool CheckRole (MsgSource source)
        {
            return this._Token.AccreditRole.IsExists(a => a == source.SystemType || a == source.SysGroup);
        }
    }
}

﻿namespace RpcSync.Service.Error
{
    public interface IErrorService
    {
        void InitError(long errorId);
    }
}
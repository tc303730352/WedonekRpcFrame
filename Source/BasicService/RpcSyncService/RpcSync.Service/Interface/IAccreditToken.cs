﻿using WeDonekRpc.Model;
using WeDonekRpc.ModularModel.Accredit;
using WeDonekRpc.ModularModel.Accredit.Model;
using RpcSync.Service.Accredit;

namespace RpcSync.Service.Interface
{
    public interface IAccreditToken
    {
        AccreditToken Token { get; }
        string AccreditId { get; }
        string CheckKey { get; }

        bool Cancel ();
        bool CheckRole (MsgSource source);
        void Create (AccreditToken token);
        bool Init (string accreditId);

        TimeSpan? Refresh ();
        bool Remove (out string[] subs);
        bool SetState (string state);

        AccreditDatum Get ();
        void Set (SetAccredit obj);
    }
}
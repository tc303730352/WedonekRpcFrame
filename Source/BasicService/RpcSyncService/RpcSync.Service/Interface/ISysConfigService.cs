﻿using WeDonekRpc.Model;

namespace RpcSync.Service.Interface
{
    public interface ISysConfigService
    {
        RemoteSysConfig GetSysConfig (MsgSource source);
    }
}
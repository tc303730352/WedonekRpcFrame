﻿namespace RpcSyncService.Model.DAL_Model
{
    [System.Serializable]
    public class ErrorLang
    {
        public string Lang { get; set; }

        public string Msg { get; set; }
    }
}

﻿namespace RpcSync.Collect.Collect
{
    public interface IContrainerCollect
    {
        long[] GetIds (long groupId);
    }
}
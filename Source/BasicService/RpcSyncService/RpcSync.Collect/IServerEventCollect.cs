﻿using RpcSync.Model;

namespace RpcSync.Collect
{
    public interface IServerEventCollect
    {
        ServiceEventDatum[] Gets (long[] ids);
        EventSwitch[] GetsEnableEvent (long serverId, long rpcMerId);
        void Refresh (long[] serverId);
    }
}
﻿using RpcSync.DAL;
using RpcSync.Model;
using WeDonekRpc.CacheClient.Interface;
using WeDonekRpc.Client.Attr;
using WeDonekRpc.Client.Interface;

namespace RpcSync.Collect.Collect
{
    [ClassLifetimeAttr(ClassLifetimeType.SingleInstance)]
    internal class RemoteServerGroupCollect : IRemoteServerGroupCollect
    {
        private readonly IIocService _Unity;
        private readonly ILocalCacheController _Cache;
        public RemoteServerGroupCollect (IIocService unity, ILocalCacheController cache)
        {
            this._Unity = unity;
            this._Cache = cache;
        }
        public long[] GetHoldServerId (long merId)
        {
            return this._Unity.Resolve<IRemoteServerGroupDAL>().GetHoldServerId(merId);
        }
        public MerServer[] GetAllServer (long merId)
        {
            string key = string.Concat("Server_", merId);
            if (this._Cache.TryGet(key, out MerServer[] servers))
            {
                return servers;
            }
            servers = this._Unity.Resolve<IRemoteServerGroupDAL>().GetAllServer(merId);
            _ = this._Cache.Set(key, servers);
            return servers;
        }

        public void Refresh (long merId)
        {
            string key = string.Concat("Server_", merId);
            _ = this._Cache.Remove(key);
        }
    }
}

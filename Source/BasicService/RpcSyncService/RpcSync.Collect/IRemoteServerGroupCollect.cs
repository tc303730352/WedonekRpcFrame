﻿using RpcSync.Model;

namespace RpcSync.Collect
{
    public interface IRemoteServerGroupCollect
    {
        long[] GetHoldServerId (long merId);
        void Refresh (long merId);
        MerServer[] GetAllServer (long merId);
    }
}
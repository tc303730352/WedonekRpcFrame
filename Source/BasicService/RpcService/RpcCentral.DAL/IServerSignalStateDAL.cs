﻿using RpcService.Model.DAL_Model;

namespace RpcCentral.DAL
{
    public interface IServerSignalStateDAL
    {
        bool SyncState(ServerSignalState[] states);
    }
}
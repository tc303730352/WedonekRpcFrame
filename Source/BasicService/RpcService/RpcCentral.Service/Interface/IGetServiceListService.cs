﻿using WeDonekRpc.Model;

namespace RpcCentral.Service.Interface
{
    public interface IGetServiceListService
    {
        GetServerListRes GetServerList(GetServerList obj, Source source);
    }
}
﻿using WeDonekRpc.Model;

namespace RpcCentral.Service.Interface
{
    public interface ITokenService
    {
        AccessTokenRes ApplyOAuthToken(ApplyAccessToken apply);
    }
}
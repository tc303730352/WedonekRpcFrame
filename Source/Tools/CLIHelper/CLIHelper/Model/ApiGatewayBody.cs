﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RpcHelper;

namespace CLIHelper.Model
{
    internal class ApiGatewayBody
    {
        private ApiServiceBody _Services;
        private GatewayApiBody[] _ApiList;
        public ApiGatewayBody(ApiServiceBody service)
        {
            _Services = service;
            _ApiList = service.ApiList.ConvertAll(a => new GatewayApiBody(a, service));
        }

        public void Save(DirectoryInfo apiDir, DirectoryInfo modelDir)
        {
            _ApiList.ForEach(a => a.SaveModel(modelDir));
            string text = _CreateApi();
            string path = Path.Combine(apiDir.FullName, string.Concat(this._Services.RootName, "Api.cs"));
            Tools.WriteText(path, text, Encoding.UTF8);
        }
        private string _CreateApi()
        {
            StringBuilder str = new StringBuilder();
            List<string> use = new List<string>()
                        {
                                "ApiGateway.Attr",
                                "HttpApiGateway",
                                "RpcHelper.Validate",
                                "RpcClient",
                               string.Concat(this._Services.Namespace,".Interface")
                        };
            this._ApiList.ForEach(a =>
            {
                if (a.Use.Count > 0)
                {
                    use.AddRange(a.Use);
                }
                use.Add(a._body.Return.Type.Namespace);
            });
            string[] us = use.Distinct().ToArray();
            us.ForEach(a =>
            {
                str.AppendFormat("using {0};\r\n", a);
            });
            str.AppendLine();

            str.AppendFormat("namespace {0}.Api\r\n", this._Services.Namespace);
            str.AppendLine("{");
            str.AppendFormat("\tinternal class {0}Api : ApiController\r\n", this._Services.RootName);
            str.AppendLine("\t{");
            str.AppendFormat("\t\tprivate I{0} _Service;\r\n\t\tpublic {1}Api(I{0} service)\r\n", this._Services.ServiceName, this._Services.RootName);
            str.AppendLine("\t\t{\r\n\t\t\tthis._Service=service;\r\n\t\t}");
            this._ApiList.ForEach(a =>
            {
                a.WriteMethod(str);
                str.AppendLine();
            });
            str.AppendLine("\t}");
            str.AppendLine("}");
            return str.ToString();
        }

    }
}

﻿using System.IO;
using System.Text;
using RpcHelper;

namespace CLIHelper.Model
{
        internal class GatewayConfigModel
        {
                public static void Save (string name, DirectoryInfo dir, string rootName)
                {
                        StringBuilder str = new StringBuilder ();
                        str.AppendLine ("using HttpApiGateway;");
                        str.AppendLine ();
                        str.AppendFormat ("namespace {0}\r\n", name);
                        str.AppendLine ("{");
                        str.AppendFormat ("\tinternal class {0}_ApiModular : BasicApiModular\r\n", rootName);
                        str.AppendLine ("\t{");
                        str.AppendFormat ("\t\tpublic {0}_ApiModular():base(\"{0}_Modular\")\r\n", rootName);
                        str.AppendLine ("\t\t{\r\n\t\t}");
                        str.AppendLine ("\t\tprotected override void Init()\r\n\t\t{\r\n\t\t\tthis.Config.ApiRouteFormat=\"/api/{controller}/{name}\";\r\n\t\t}");
                        str.AppendLine("\t}");
                        str.AppendLine("}");
                        string path = Path.Combine (dir.FullName, string.Concat (rootName, "_ApiModular.cs"));
                        Tools.WriteText (path, str.ToString (), Encoding.UTF8);
                }
}
}

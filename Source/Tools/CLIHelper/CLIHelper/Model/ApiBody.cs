﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using CLIHelper.Helper;
using RpcHelper;

namespace CLIHelper.Model
{
    internal class ApiBody
    {
        public ApiBody(Type type)
        {
            this.Source = type;
            this.ApiName = Helper.Helper.GetApiName(type);
            this.Return = Helper.Helper.GetReturnType(type, out bool isPaging);
            this.IsPaging = isPaging;
            this.Show = XmlShowHelper.FindParamShow(type);
            List<string> use = new List<string>
                        {
                                type.Namespace,
                                this.Return.Type.Namespace,
                        };

            PropertyInfo[] pros = type.GetProperties().FindAll(a => Helper.Helper.CheckProperty(a));
            this.Params = pros.Convert(a =>
            {
                if (this.IsPaging && a.DeclaringType.Name == ConstDic._BasicPageType.Name)
                {
                    return null;
                }
                return new ApiParam(a);
            });
            if (this.IsPaging)
            {
                this.Params = this.Params.Add(new ApiParam[] {
                                        new ApiParam("paging"),
                                        new ApiParam("count"),
                                });
                use.Add("RpcModel");
            }
            this.Params.ForEach(a =>
            {
                use.Add(a.Type.Namespace);
            });
            this.UseList = use.Distinct().ToArray();
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsPaging
        {
            get;
            set;
        }
        public string[] UseList
        {
            get;
            set;
        }
        public string ApiName
        {
            get;
            set;
        }
        public string Show
        {
            get;
            set;
        }
        public ReturnTypeBody Return
        {
            get;
            set;
        }
        public ApiParam[] Params
        {
            get;
            set;
        }
        public Type Source
        {
            get;
            set;
        }
        public void WriteBody(StringBuilder str)
        {
            StringBuilder arg = new StringBuilder();
            this.Params.ForEach(a =>
            {
                arg.Append(",");
                arg.Append(a.ToString());
            });
            if (arg.Length > 0)
            {
                arg.Remove(0, 1);
            }
            Helper.Helper.WriteShow(this, str);
            str.AppendFormat("\t\t{0} {1}({2});\r\n", this.Return.ToString(), this.ApiName, arg.ToString());

        }

        public void WriteMothed(StringBuilder str)
        {
            StringBuilder arg = new StringBuilder();
            this.Params.ForEach(a =>
            {
                arg.Append(",");
                arg.Append(a.ToString());
            });
            if (arg.Length > 0)
            {
                arg.Remove(0, 1);
            }
            str.AppendFormat("\t\tpublic {0} {1}({2})\r\n", this.Return.ToString(), this.ApiName, arg.ToString());
            str.AppendLine("\t\t{");
            str.Append("\t\t\t");
            if (this.Return.Type != ConstDic._VoidType)
            {
                str.Append("return ");
            }
            str.AppendFormat("new {0}", this.Source.Name);
            if (this.Params.Length == 0)
            {
                str.Append("()");
            }
            else
            {
                str.Append("\r\n\t\t\t");
                str.AppendLine("{");
                this.Params.ForEach(a =>
                {
                    a.InitParam(str);
                });
                str.Remove(str.Length - 1, 1);
                str.Append("\t\t\t}");
            }
            if (this.IsPaging)
            {
                str.AppendLine(".Send(out count);");
            }
            else
            {
                str.AppendLine(".Send();");
            }
            str.AppendLine("\t\t}");
        }
    }
}

﻿using System;
using RpcClient;
using RpcModel;

namespace CLIHelper
{
    internal class ConstDic
    {
        public static readonly Type _AppIdAttr = typeof(AppIdentityAttr);
        public static readonly Type _BasicPage = typeof(BasicPage);
        public static readonly Type _UpFileType = typeof(RpcUpFile);
        public static readonly Type _VoidType = typeof(void);
        public static readonly Type _RpcRemoteType = typeof(RpcRemote);
        public static readonly Type _RpcBroadcastType = typeof(RpcBroadcast);
        public static readonly Type _BasicPageType = typeof(BasicPage<>);
        public static readonly Type _objectType = typeof(object);
        public static readonly Type _IConfigType = typeof(IRemoteConfig);

        public static readonly Type _IBroadcastType = typeof(IRemoteBroadcast);
        public static string CsprojTemplate = "<Project Sdk=\"Microsoft.NET.Sdk\"><PropertyGroup><TargetFramework>net6.0</TargetFramework></PropertyGroup>" +
                "<PropertyGroup Condition=\"'$(Configuration)|$(Platform)'=='Debug|AnyCPU'\"><DocumentationFile>{1}.xml</DocumentationFile></PropertyGroup>" +
                "<PropertyGroup Condition=\"'$(Configuration)|$(Platform)'=='Release|AnyCPU'\"><DocumentationFile>{1}.xml</DocumentationFile></PropertyGroup>" +
                "<ItemGroup>" +
                "<Reference Include=\"ApiGateway\"><HintPath>..\\Reference\\ApiGateway.dll</HintPath></Reference>" +
                "<Reference Include=\"RpcClient\"><HintPath>..\\Reference\\RpcClient.dll</HintPath></Reference>" +
                "<Reference Include=\"HttpApiGateway\"><HintPath>..\\Reference\\HttpApiGateway.dll</HintPath></Reference>" +
                "<Reference Include=\"RpcHelper\"><HintPath>..\\Reference\\RpcHelper.dll</HintPath></Reference>" +
                "<Reference Include=\"RpcModel\"><HintPath>..\\Reference\\RpcModel.dll</HintPath></Reference>" +
                "<Reference Include=\"{0}\"><HintPath>..\\Reference\\{0}.dll</HintPath></Reference>" +
                "</ItemGroup ></Project>";
    }
}

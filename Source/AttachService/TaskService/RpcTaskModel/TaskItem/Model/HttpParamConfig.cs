﻿using System;
using WeDonekRpc.Helper;
using WeDonekRpc.Helper.Http;
using WeDonekRpc.Helper.Validate;
namespace RpcTaskModel.TaskItem.Model
{
    /// <summary>
    /// Http配置参数
    /// </summary>
    public class HttpParamConfig
    {
        /// <summary>
        /// 请求的完整URI
        /// </summary>
        [NullValidate("task.item.http.uri.null")]
        public Uri Uri
        {
            get;
            set;
        }
        /// <summary>
        /// 请求方式
        /// </summary>
        [NullValidate("task.item.http.method.null")]
        public string RequestMethod
        {
            get;
            set;
        }
        /// <summary>
        /// 请求类型
        /// </summary>
        [EnumValidate("task.item.http.reqType.error", typeof(HttpReqType))]
        public HttpReqType ReqType
        {
            get;
            set;
        }
        /// <summary>
        /// 模拟请求类型
        /// </summary>
        [EnumValidate("task.item.http.analogType.error", typeof(AnalogType))]
        public AnalogType AnalogType
        {
            get;
            set;
        }
        public Uri Referer
        {
            get;
            set;
        }
        /// <summary>
        /// Post参数
        /// </summary>
        public string PostParam
        {
            get;
            set;
        }

    }
}

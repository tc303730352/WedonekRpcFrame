﻿using WeDonekRpc.Client;
using WeDonekRpc.Client.Interface;

namespace WeDonekRpc.Modular
{
    public class ExtendModular : IRpcInitModular
    {
        public void Init (IIocService ioc)
        {
            IRpcDirectShieldPlugIn shieId = ioc.Resolve<IRpcDirectShieldPlugIn>();
            shieId.Init();
        }
        public void Load (RpcInitOption option)
        {
            option.LoadModular<RpcCacheModular.CacheModular>();
        }
        public void InitEnd (IIocService service)
        {
        }
    }
}

﻿using System.Collections.Generic;
using WeDonekRpc.Modular.SysEvent.Model;
using WeDonekRpc.Client.Attr;
using WeDonekRpc.Client.Interface;

namespace WeDonekRpc.Modular
{
    [ClassLifetimeAttr(ClassLifetimeType.SingleInstance)]
    public interface IRpcEventService
    {
        void Init (IRpcService service);
        void AddLog (BasicEvent ev, Dictionary<string, string> args);
        void AddLog (long evId, Dictionary<string, string> args);
    }
}
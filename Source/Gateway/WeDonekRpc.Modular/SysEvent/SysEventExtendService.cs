﻿using WeDonekRpc.Client.Attr;
using WeDonekRpc.Client.Interface;

namespace WeDonekRpc.Modular.SysEvent
{
    [IocName("SysEventService")]
    internal class SysEventExtendService : IRpcExtendService
    {
        private readonly IRpcEventService _Service;
        public SysEventExtendService (IRpcEventService service)
        {
            this._Service = service;
        }
        public void Load (IRpcService service)
        {
            this._Service.Init(service);
        }

    }
}

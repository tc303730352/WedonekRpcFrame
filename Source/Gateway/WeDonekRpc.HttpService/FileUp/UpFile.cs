﻿using System.IO;

using WeDonekRpc.HttpService.Helper;
using WeDonekRpc.HttpService.Interface;

namespace WeDonekRpc.HttpService
{
    /// <summary>
    /// 上传文件信息
    /// </summary>
    public class UpFile : IUpFile
    {
        private UpFileParam _File;
        internal UpFile(UpFileParam param, string savePath, long size)
        {
            this.FileSize = size;
            this._File = param;
            this._TempFilePath = savePath;
        }
        private readonly string _TempFilePath = null;

        /// <summary>
        /// 文件大小
        /// </summary>
        public long FileSize { get; }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName => _File.FileName;
        /// <summary>
        /// 文件类型
        /// </summary>
        public string FileType => _File.Name;
        /// <summary>
        /// 内容类型
        /// </summary>
        public string ContentType => _File.ContentType;
        /// <summary>
        /// 临时文件保存路径
        /// </summary>
        internal string TempFilePath => this._TempFilePath;

        public string FileMd5 => _File.FileMd5;

        public byte FileCs => _File.FileCs;

        /// <summary>
        /// 读取流
        /// </summary>
        /// <returns></returns>
        public byte[] ReadStream()
        {
            byte[] myByte = new byte[this.FileSize];
            Stream stream = this.GetStream();
            stream.Position = 0;
            stream.Read(myByte, 0, myByte.Length);
            return myByte;
        }
        public long CopyStream(Stream stream, int offset)
        {
            Stream file = this.GetStream();
            file.Position = 0;
            stream.Position = offset;
            file.CopyTo(stream);
            return this.FileSize;
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="savePath"></param>
        public string SaveFile(string savePath)
        {
            savePath = FileHelper.GetFileFullPath(savePath);
            if (this._Stream != null)
            {
                this._Stream.Close();
                this._Stream.Dispose();
            }
            FileInfo file = new FileInfo(savePath);
            if (file.Exists)
            {
                return savePath;
            }
            else if (!file.Directory.Exists)
            {
                file.Directory.Create();
            }
            File.Move(this._TempFilePath, savePath);
            return savePath;
        }

        public void Dispose()
        {
            if (this._Stream != null)
            {
                this._Stream.Close();
                this._Stream.Dispose();
            }
            if (File.Exists(this._TempFilePath))
            {
                File.Delete(this._TempFilePath);
            }
        }
        private Stream _Stream = null;
        public Stream GetStream()
        {
            if (this._Stream == null)
            {
                this._Stream = File.Open(this._TempFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            return this._Stream;
        }
    }
}

﻿using System.IO;

using WeDonekRpc.HttpService.Interface;

namespace WeDonekRpc.HttpService
{
    internal interface ISyncUpFileHelper
    {
        void LoadFile(Stream stream, IUpFileRequest request);
    }
}
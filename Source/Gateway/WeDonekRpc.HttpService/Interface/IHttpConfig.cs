﻿using System;
using System.Net;
using System.Text;
using WeDonekRpc.HttpService.Config;

namespace WeDonekRpc.HttpService.Interface
{
    public interface IHttpConfig
    {
        ICrossConfig Cross { get; }
        string CertHashVal { get; }
        HttpFileConfig File { get; }
        GzipConfig Gzip { get; }
        LogConfig Log { get; }
        UpConfig Up { get; }
        Uri RealRequestUri { get; }

        /// <summary>
        /// 响应编码
        /// </summary>
        Encoding ResponseEncoding { get; }

        /// <summary>
        /// 请求编码
        /// </summary>
        Encoding RequestEncoding { get; }

        bool IgnoreWriteExceptions { get; }

        TimeOutConfig TimeOut { get; }
        string Realm { get; }
        AuthenticationSchemes AuthenticationSchemes { get; }
        void AddFileDir (HttpFileConfig config);

    }
}
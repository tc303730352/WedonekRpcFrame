﻿using System;
using System.IO;
namespace WeDonekRpc.HttpService.Interface
{
    public interface IUpFileRequest
    {
        bool IsGenerateMd5 { get; }
        void CheckUpFile(UpFileParam param);
        Stream GetSaveStream(UpFileParam param);
        /// <summary>
        /// 文件上传请求
        /// </summary>
        void InitFileUp();
        void SaveUpStream(UpFileParam upParam, Stream stream);
        void UpFail(Exception e);
        void VerificationFile(UpFileParam upParam, long length);
    }
}

﻿using System;

namespace WeDonekRpc.ApiGateway.Attr
{
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
    public class ApiHeadParam:Attribute
    {
        public string Name { get; set; }
    }
}

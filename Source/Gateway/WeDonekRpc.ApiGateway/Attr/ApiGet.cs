﻿using System;

namespace WeDonekRpc.ApiGateway.Attr
{
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
    public class ApiGet : Attribute
    {
        public string Name { get; set; }
    }
}

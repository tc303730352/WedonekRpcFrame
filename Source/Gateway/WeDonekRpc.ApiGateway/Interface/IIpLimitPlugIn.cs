﻿using WeDonekRpc.ApiGateway.Interface;

namespace WeDonekRpc.ApiGateway.Collect
{
    public interface IIpLimitPlugIn : IPlugIn
    {
        bool IsLimit(string ip);
    }
}
﻿using System;
using System.Reflection;
using WeDonekRpc.ApiGateway.Attr;
using WeDonekRpc.Helper;

namespace WeDonekRpc.ApiGateway.Helper
{
    public class ApiParamHelper
    {

        public static string GetReceiveMethod(ParameterInfo type, string def, ref string name)
        {
            object[] list = type.GetCustomAttributes(false);
            if (list.IsNull())
            {
                return def;
            }
            foreach (object i in list)
            {
                if (i is ApiPostForm post)
                {
                    if (post.Name != null)
                    {
                        name = post.Name;
                    }
                    return "PostForm";
                }
                else if (i is ApiGet get)
                {
                    if (get.Name != null)
                    {
                        name = get.Name;
                    }
                    return "GET";
                }
                else if (i is ApiHeadParam head)
                {
                    if (head.Name != null)
                    {
                        name = head.Name;
                    }
                    return "Head";
                }
                else if (i is ApiRouteParam route)
                {
                    if (route.Name != null)
                    {
                        name = route.Name;
                    }
                    return "Route";
                }
            }
            return def;
        }

    }
}

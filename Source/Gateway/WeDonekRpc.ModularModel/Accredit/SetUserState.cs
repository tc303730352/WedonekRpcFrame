﻿using WeDonekRpc.Client;

using WeDonekRpc.Model;

namespace WeDonekRpc.ModularModel.Accredit
{
    [IRemoteConfig("sys.sync", Transmit = "Accredit")]
    public class SetUserState : RpcRemote<SetUserStateRes>
    {
        [TransmitColumn(TransmitType.ZoneIndex)]
        public string AccreditId
        {
            get;
            set;
        }
        public string UserState
        {
            get;
            set;
        }
        public long VerNum { get; set; }
        /// <summary>
        /// 是否强制覆盖
        /// </summary>
        public bool IsCover { get; set; }
    }
}

﻿using WeDonekRpc.Client.Attr;
using WeDonekRpc.Helper;
using WeDonekRpc.HttpApiGateway.FileUp;
using WeDonekRpc.HttpApiGateway.FileUp.Interface;
using WeDonekRpc.HttpApiGateway.FileUp.Model;
using WeDonekRpc.HttpApiGateway.Interface;
using WeDonekRpc.HttpService.Interface;
using WeDonekRpc.Modular;

namespace WeDonekRpc.HttpApiGateway
{
    /// <summary>
    /// 分块文件上传控制器
    /// </summary>
    [IgnoreIoc]
    public class UpBlockFileController : IUpBlockFileController, IUpBlockFile
    {
        private IApiService _Service;
        public UpFileData File
        {
            get;
            private set;
        }
        /// <summary>
        /// 服务名
        /// </summary>
        public string ServiceName => this._Service.ServiceName;

        /// <summary>
        /// 用户状态
        /// </summary>
        public IUserState UserState => this._Service.UserState;

        /// <summary>
        /// 请求
        /// </summary>
        public IHttpRequest Request => this._Service.Request;



        public virtual void Complate (IUpFileResult result)
        {
        }

        public UpBasicFile Init (IApiService service, IBlockTask task)
        {
            this._Service = service;
            this.File = service.Request.GetPostObject<UpFileData>();
            IUpTask block = new BlockUp(task);
            this.InitUp(block);
            if (block.UserDir.IsNull())
            {
                throw new ErrorException("gateway.http.up.userdir.error");
            }
            return new UpBasicFile
            {
                FileMd5 = this.File.FileMd5,
                FileSize = this.File.FileSize,
                DirName = block.UserDir,
                FileName = this.File.FileName
            };
        }


        public virtual void InitUp (IUpTask task)
        {

        }
    }
    [IgnoreIoc]
    public class UpBlockFileController<T> : IUpBlockFileController<T>, IUpBlockFile
    {
        private IApiService _Service;
        public UpFileDatum<T> File
        {
            get;
            private set;
        }
        /// <summary>
        /// 服务名
        /// </summary>
        public string ServiceName => this._Service.ServiceName;

        /// <summary>
        /// 用户状态
        /// </summary>
        public IUserState UserState => this._Service.UserState;

        /// <summary>
        /// 请求
        /// </summary>
        public IHttpRequest Request => this._Service.Request;



        public virtual void Complate (IUpFileResult result)
        {
        }

        public UpBasicFile Init (IApiService service, IBlockTask task)
        {
            this._Service = service;
            this.File = service.Request.GetPostObject<UpFileDatum<T>>();
            IUpTask block = new BlockUp(task);
            this.InitUp(block);
            if (block.UserDir.IsNull())
            {
                throw new ErrorException("gateway.http.up.userdir.error");
            }
            return new UpBasicFile
            {
                FileMd5 = this.File.FileMd5,
                FileSize = this.File.FileSize,
                DirName = block.UserDir,
                FileName = this.File.FileName
            };
        }

        public virtual void InitUp (IUpTask task)
        {

        }
    }
}

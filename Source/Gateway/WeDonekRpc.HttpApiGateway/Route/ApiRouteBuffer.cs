﻿using WeDonekRpc.HttpApiGateway.Interface;
using WeDonekRpc.HttpApiGateway.Model;
using System;
using System.Collections.Generic;

namespace WeDonekRpc.HttpApiGateway.Route
{
    internal class ApiRouteBuffer
    {
        public Type Form
        {
            get;
            set;
        }
        public Type To
        {
            get;
            set;
        }
        public string Name { get; set; }

        public IRouteConfig Config { get; set; }

        public List<ApiBody> Apis { get; set; } = new List<ApiBody>();
    }
}

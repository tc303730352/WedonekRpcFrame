﻿using WeDonekRpc.HttpApiGateway.FileUp.Interface;

namespace WeDonekRpc.HttpApiGateway.FileUp
{
    internal class BlockUp : IUpTask
    {
        private readonly IBlockTask _Task;
        public BlockUp(IBlockTask task)
        {
            this._Task = task;
        }

        public string UserDir { get; private set; }

        public void SetUserDirName(string dirName)
        {
            this.UserDir = dirName;
        }

        public void UpComplate<T>(T result)
        {
            this._Task.UpComplate(result);
        }

        public void UpError(string error)
        {
            this._Task.UpError(error);
        }
    }
}

﻿using System.IO;
using WeDonekRpc.HttpApiGateway.FileUp.Model;
using WeDonekRpc.HttpApiGateway.Interface;
using WeDonekRpc.HttpService.Interface;

namespace WeDonekRpc.HttpApiGateway.FileUp.Interface
{
    internal interface IBlockUpTask: IBlockTask,System.IDisposable
    {
        string TaskId { get; }

        string TaskKey { get; }
        int TimeOut { get; }
        string ServerName { get; }
        BlockUpSate GetUpState();
        void Load(IApiService service);
        void WriteUpFile(IUpFile file, int index);
        bool CheckBlockIndex(int blockIndex);
        Stream GetStream(int index);
    }
}
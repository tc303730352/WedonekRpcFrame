﻿using System.IO;
using WeDonekRpc.Client.Attr;

namespace WeDonekRpc.HttpApiGateway.FileUp.Interface
{
    [IgnoreIoc]
    public interface IUpFileResult
    {
        /// <summary>
        /// 上传完成并返回
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        void UpComplate<T> (T result);
        /// <summary>
        /// 设置上传的错误状态
        /// </summary>
        /// <param name="error"></param>
        void UpError (string error);
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="path"></param>
        FileInfo Save (string path);

    }
}

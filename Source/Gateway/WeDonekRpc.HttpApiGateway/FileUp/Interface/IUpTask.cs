﻿using WeDonekRpc.Client.Attr;

namespace WeDonekRpc.HttpApiGateway.FileUp.Interface
{
    [IgnoreIoc]
    public interface IUpTask
    {
        string UserDir { get; }
        /// <summary>
        /// 上传完成并返回
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        void UpComplate<T> (T result);
        /// <summary>
        /// 设置上传的错误状态
        /// </summary>
        /// <param name="error"></param>
        void UpError (string error);
        /// <summary>
        /// 设置用户临时目录
        /// </summary>
        /// <param name="dirName"></param>
        void SetUserDirName (string dirName);
    }

}

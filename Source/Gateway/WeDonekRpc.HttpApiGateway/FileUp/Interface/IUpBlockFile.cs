﻿using WeDonekRpc.Client.Attr;
using WeDonekRpc.HttpApiGateway.FileUp.Model;
using WeDonekRpc.HttpApiGateway.Interface;

namespace WeDonekRpc.HttpApiGateway.FileUp.Interface
{
    [IgnoreIoc]
    internal interface IUpBlockFile : IApiGateway
    {
        UpBasicFile Init (IApiService service, IBlockTask task);

        void Complate (IUpFileResult result);
    }
}

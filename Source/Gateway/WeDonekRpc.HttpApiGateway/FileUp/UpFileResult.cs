﻿using System;
using System.IO;
using WeDonekRpc.HttpApiGateway.FileUp.Interface;

namespace WeDonekRpc.HttpApiGateway.FileUp
{
    internal class UpFileResult : IUpFileResult
    {
        private readonly IBlockTask _Task;
        public UpFileResult(IBlockTask task)
        {
            this._Task = task;
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="path"></param>
        public FileInfo Save(string path)
        {
            return this._Task.Save(path);
        }
        /// <summary>
        /// 上传完成
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void UpComplate<T>(T result)
        {
            this._Task.UpComplate(result);
        }

        public void UpError(string error)
        {
            this._Task.UpError(error);
        }

    }
}

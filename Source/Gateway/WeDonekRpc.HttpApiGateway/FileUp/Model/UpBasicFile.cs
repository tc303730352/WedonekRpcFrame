﻿namespace WeDonekRpc.HttpApiGateway.FileUp.Model
{
    public class UpBasicFile
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName
        {
            get;
            set;
        }
        /// <summary>
        /// 存储的目录名
        /// </summary>
        public string DirName
        {
            get;
            set;
        }
        /// <summary>
        /// 文件大小
        /// </summary>
        public long FileSize
        {
            get;
            set;
        }
        /// <summary>
        /// 文件MD5
        /// </summary>
        public string FileMd5
        {
            get;
            set;
        }
    }
}

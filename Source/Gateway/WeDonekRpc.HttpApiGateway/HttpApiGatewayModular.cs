﻿using WeDonekRpc.HttpService.Interface;
using WeDonekRpc.Client;
using WeDonekRpc.Client.Interface;

namespace WeDonekRpc.HttpApiGateway
{
    internal class HttpApiGatewayModular : IRpcInitModular
    {
        public void Init (IIocService ioc)
        {

        }

        public void Load (RpcInitOption option)
        {
            _ = option.Ioc.RegisterInstance<ICrossConfig>(HttpService.HttpService.Config.Cross);
            ApiGatewayService.Init();
            option.Load("ApiGateway");
        }
        public void InitEnd (IIocService service)
        {
        }
    }
}

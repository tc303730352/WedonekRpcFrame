﻿using System.Collections.Generic;
using System.Reflection;

using WeDonekRpc.HttpApiGateway.Model;

using WeDonekRpc.Helper;

namespace WeDonekRpc.HttpApiGateway.Helper
{
    internal class FuncHelper
    {

        public static FuncParam[] InitMethod (MethodInfo method)
        {
            List<int> rets = [];
            FuncParam[] list = method.GetParameters().ConvertAll(a => ApiHelper.GetParamType(a));
            if (list.IsExists(b => b.ParamType == FuncParamType.参数 && !b.IsBasicType))
            {
                list.ForEach(a => a.ParamType == FuncParamType.参数 && a.IsBasicType, a =>
                {
                    a.ReceiveMethod = "GET";
                });
            }
            return list;
        }
    }
}

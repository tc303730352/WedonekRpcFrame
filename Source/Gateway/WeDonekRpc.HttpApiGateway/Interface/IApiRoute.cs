﻿using WeDonekRpc.ApiGateway.Model;
using WeDonekRpc.HttpApiGateway.Interface;
using WeDonekRpc.Client.Ioc;
using System.Reflection;

namespace WeDonekRpc.HttpApiGateway
{
    internal interface IApiRoute : IRoute, System.IDisposable
    {
        bool IsRegex { get; }

        bool IsEnable { get; }

        void Enable();

        void Disable();
        
        string ApiEventType { get;  }
        /// <summary>
        /// 获取上传的配置信息
        /// </summary>
        /// <returns></returns>
        ApiUpSet GetUpSet();
        /// <summary>
        /// 获取上传配置
        /// </summary>
        /// <returns></returns>
        IUpFileConfig CreateUpFileConfig(IocScope scope);

        MethodInfo Source { get; }

        void ExecApi(IService service);
        void ReceiveRequest(IService service);

        void InitRoute();
    }
}
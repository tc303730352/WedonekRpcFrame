﻿using System;
using WeDonekRpc.Helper;
using WeDonekRpc.HttpApiGateway.Model;

namespace WeDonekRpc.HttpApiGateway.Interface
{
    internal interface IService : IApiService
    {
        bool IsError { get; }

        string LastError { get; }
        void InitService (IApiModular modular);
        void ReplyError (string error);
        void ReplyError (ErrorException error);
        void Reply ();
        void Reply (object result);
        void Reply (IResponse response);
        void ReplyError (string show, Exception e);
        void CheckAccredit (ApiAccreditSet accreditSet);
        bool CheckCache (string etag, string toUpdateTime);
        void Dispose ();
        void End ();
        void InitIdentity ();
    }
}

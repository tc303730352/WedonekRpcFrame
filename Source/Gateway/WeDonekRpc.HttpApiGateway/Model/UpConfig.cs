﻿using System;
using WeDonekRpc.ApiGateway.Model;
using WeDonekRpc.Client;
using WeDonekRpc.Helper.Config;

namespace WeDonekRpc.HttpApiGateway.Model
{
    /// <summary>
    /// 上传配置
    /// </summary>
    public class UpConfig
    {
        public UpConfig ()
        {
            RpcClient.Config.GetSection("gateway:up").AddRefreshEvent(this._Refresh);
        }

        private void _Refresh (IConfigSection section, string name)
        {
            this.SingleFileSize = section.GetValue<int>("SingleFileSize", 1024 * 1024 * 10);
            this.LimitFileNum = section.GetValue<int>("LimitFileNum", 1);
            this.BlockUpSize = section.GetValue<int>("BlockUpSize", 10 * 1024 * 1024);
            this.SyncBlockLimit = section.GetValue<int>("SyncBlockLimit", 1024 * 1024 * 1024);
            this.FileExtension = section.GetValue<string[]>("FileExtension", Array.Empty<string>());
            this.MaxCheckBlock = section.GetValue<int>("MaxCheckBlock", 2);
            this.BlockUpUri = section.GetValue<string>("BlockUpUri", "/api/file/block/up").ToLower();
            this.BlockUpUriIsRegex = section.GetValue<bool>("BlockUpUriIsRegex", false);
            this.BlockUpOverTime = section.GetValue<int>("BlockUpOverTime", 30);
            this.EnableBlock = section.GetValue<bool>("EnableBlock", true);
        }

        /// <summary>
        /// 单文件上传大小
        /// </summary>
        public int SingleFileSize
        {
            get;
            private set;
        }
        /// <summary>
        /// 允许的扩展
        /// </summary>
        public string[] FileExtension
        {
            get;
            private set;
        }
        /// <summary>
        /// 限定一次请求上传文件数量（0 无限制）
        /// </summary>
        public int LimitFileNum
        {
            get;
            private set;
        }
        /// <summary>
        /// 分块上传大小
        /// </summary>
        public int BlockUpSize
        {
            get;
            private set;
        }
        /// <summary>
        /// 同步分块上传校验 大小(默认1G) 文件超过1G采取异步分块
        /// </summary>
        public int SyncBlockLimit
        {
            get;
            private set;
        }
        /// <summary>
        /// 启用分包上传
        /// </summary>
        public bool EnableBlock { get; private set; } = true;
        /// <summary>
        /// 续传时临时文件最大校验的快(检查文件) 0=关闭校验 -1=无限制
        /// </summary>
        public int MaxCheckBlock { get; private set; } = 2;
        /// <summary>
        /// 分块上传URI地址
        /// </summary>
        public string BlockUpUri
        {
            get;
            private set;
        }
        /// <summary>
        /// 分块上传URI地址是否是正则表达式
        /// </summary>
        public bool BlockUpUriIsRegex
        {
            get;
            private set;
        }
        /// <summary>
        /// 分块上传超时时间
        /// </summary>
        public int BlockUpOverTime
        {
            get;
            private set;
        }
        internal ApiUpSet ToUpSet ()
        {
            return new ApiUpSet
            {
                BlockUpSize = this.BlockUpSize,
                Extension = this.FileExtension,
                LimitFileNum = this.LimitFileNum,
                MaxSize = this.SingleFileSize,
                TempFileSaveDir = HttpService.HttpService.Config.File.TempDirPath
            };
        }
    }
}

﻿using WeDonekRpc.Helper;
using WeDonekRpc.Helper.Validate;
using WeDonekRpc.HttpApiGateway.Collect;
using WeDonekRpc.HttpApiGateway.FileUp.Interface;
using WeDonekRpc.HttpApiGateway.FileUp.Model;
namespace WeDonekRpc.HttpApiGateway.Api
{
    /// <summary>
    /// 大文件分块上传
    /// </summary>
    internal class BlockUpController : ApiController
    {
        /// <summary>
        /// 获取分块上传任务的状态
        /// </summary>
        /// <param name="taskId">任务ID</param>
        /// <returns></returns>
        /// <exception cref="ErrorException"></exception>
        public BlockUpSate GetState ([NullValidate("gateway.http.up.taskId.error")] string taskId)
        {
            if (BlockUpCollect.GetTask(taskId, out IBlockUpTask task))
            {
                return task.GetUpState();
            }
            throw new ErrorException("gateway.http.up.task.not.find");
        }
    }
}

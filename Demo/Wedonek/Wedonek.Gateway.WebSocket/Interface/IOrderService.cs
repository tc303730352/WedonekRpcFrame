﻿using System;
using Wedonek.Demo.RemoteModel.Order.Model;
using Wedonek.Gateway.WebSocket.Model;
using WeDonekRpc.ApiGateway.Interface;
using WeDonekRpc.Client;
using WeDonekRpc.Model;
using WeDonekRpc.Modular;

namespace Wedonek.Gateway.WebSocket.Interface
{
    public interface IOrderService
    {
        Guid AddOrder (IUserState state, OrderParam add, IClientIdentity identity);
        void DropOrder (Guid orderId);
        OrderData GetOrder (Guid orderId);
        PagingResult<OrderData> Query (IUserState state, BasicPage paging);
    }
}
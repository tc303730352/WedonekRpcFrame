﻿using System;
using WeDonekRpc.Client;
using WeDonekRpc.Model;

namespace Wedonek.Demo.RemoteModel.Order
{
    [IRemoteConfig("demo.order")]
    public class DropOrder : RpcRemote
    {
        public Guid OrderId
        {
            get;
            set;
        }
    }
}

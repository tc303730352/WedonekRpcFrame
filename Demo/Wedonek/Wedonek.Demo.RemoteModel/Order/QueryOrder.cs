﻿using WeDonekRpc.Client;
using WeDonekRpc.Model;
namespace Wedonek.Demo.RemoteModel.Order
{
    [IRemoteConfig("demo.order")]
    public class QueryOrder : BasicPage<Model.OrderData>
    {
        public long UserId
        {
            get;
            set;
        }
    }
}

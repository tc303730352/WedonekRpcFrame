﻿using System;
using System.IO;
using Wedonek.Gateway.Modular.Model;
using WeDonekRpc.ApiGateway.Attr;
using WeDonekRpc.HttpApiGateway;
using WeDonekRpc.HttpApiGateway.FileUp.Interface;
using WeDonekRpc.HttpService;

namespace Wedonek.Gateway.Modular.FileUp
{
    /// <summary>
    /// 分块上传Demo
    /// </summary>
    [ApiRouteName("/api/block/up")]
    internal class BlockUpDemo : UpBlockFileController<UpFileArg>
    {
        public override void InitUp (IUpTask task)
        {
            //设置用户分块上传的临时文件存放目录名称(已临时目录做文件来源区分)
            task.SetUserDirName(this.UserState.AccreditId);
        }

        public override void Complate (IUpFileResult result)
        {
            string name = Guid.NewGuid().ToString("N") + Path.GetExtension(this.File.FileName);
            string savePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "UpFile", name);
            _ = result.Save(savePath);
            result.UpComplate<Uri>(this.Request.GetUri("UpFile/" + name));
        }
    }
}

﻿using Wedonek.Gateway.Modular.Interface;
using Wedonek.Gateway.Modular.Model;
using WeDonekRpc.ApiGateway.Attr;
using WeDonekRpc.HttpApiGateway;

namespace Wedonek.Gateway.Modular.Controller
{
    /// <summary>
    /// DB订单(2PC Demo)
    /// </summary>
    internal class DbOrderController : ApiController
    {
        private readonly IDBOrderService _Service;

        public DbOrderController (IDBOrderService service)
        {
            this._Service = service;
        }

        public long AddOrder (OrderParam add)
        {
            return this._Service.AddOrder(add, base.UserState.GetValue<long>("UserId"));
        }
        /// <summary>
        /// 压力测试
        /// </summary>
        /// <returns>运行时间</returns>
        [ApiPrower(false)]
        public string PressureTest (int num)
        {
            return this._Service.PressureTest(1, num);
        }
    }
}

﻿using Wedonek.Gateway.Modular.Model;

namespace Wedonek.Gateway.Modular.Interface
{
    public interface IDBOrderService
    {
        string PressureTest (long userId, int num);
        long AddOrder (OrderParam add, long userId);
    }
}
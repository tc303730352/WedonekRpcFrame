﻿using System;
using Wedonek.Demo.RemoteModel.Order;
using Wedonek.Demo.Service.Interface;
using WeDonekRpc.Client;
using WeDonekRpc.Client.Interface;
using WeDonekRpc.Helper;
using WeDonekRpc.Model;
using WeDonekRpc.Modular;

namespace Wedonek.Demo.Service
{
    internal class RpcEvent : IRpcEvent
    {
        public void Load (RpcInitOption option)
        {
            option.LoadModular<ExtendModular>();
            option.Load("Wedonek.Demo.Service");
            option.Tran.RegSagaTran<AddOrder>((cur) =>
            {
                IOrderService order = RpcClient.Ioc.Resolve<IOrderService>();
                AddOrder add = cur.Body.GetBody<AddOrder>();
                if (!order.DropOrder(add.OrderNo))
                {
                    throw new ErrorException("demo.order.drop.error");
                }
            });
            option.Tran.RegSagaTran("CreateOrder", (a) =>
            {
                IOrderService order = RpcClient.Ioc.Resolve<IOrderService>();
                order.DropOrder(new Guid(a.Body.Body));
            });
        }

        public void RefreshService (long serverId)
        {
            //刷新服务节点配置事件
        }

        public void ServerClose ()
        {
            //服务关闭事件
        }

        public void ServerInit (IIocService ioc)
        {
        }

        /// <summary>
        /// 服务已启动
        /// </summary>
        public void ServerStarted ()
        {
        }

        public void ServerStarting ()
        {
            //服务启动前

        }

        public void ServiceState (RpcServiceState state)
        {

        }
    }
}

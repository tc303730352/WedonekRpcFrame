﻿using System;
using Wedonek.Demo.Service.Model;
using WeDonekRpc.Model;

namespace Wedonek.Demo.Service.Interface
{
    public interface IOrderService
    {
        Guid AddOrder (OrderAddModel order);
        void DropOrder (Guid id);
        OrderInfo GetOrder (Guid orderId);
        OrderInfo[] Query (long userId, IBasicPage paging, out int count);
        void AddOrder (OrderInfo order);
        bool DropOrder (string orderNo);
    }
}
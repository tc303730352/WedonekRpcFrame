﻿using Wedonek.Demo.Service.Model;

namespace Wedonek.Demo.Service.Interface
{
    public interface IDBOrderService
    {
        long AddOrder (OrderAddModel order);
    }
}
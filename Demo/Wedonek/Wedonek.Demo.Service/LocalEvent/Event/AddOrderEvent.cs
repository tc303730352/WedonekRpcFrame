﻿using WeDonekRpc.Client;

namespace Wedonek.Demo.Service.LocalEvent.Event
{
    public class AddOrderEvent : RpcLocalEvent
    {
        public long UserId
        {
            get;
            set;
        }
        public string SerialNo
        {
            get;
            set;
        }
    }
}

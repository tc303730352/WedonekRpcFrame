﻿using System;
using Wedonek.Demo.Service.DB;
using Wedonek.Demo.Service.Interface;
using Wedonek.Demo.Service.Model;
using WeDonekRpc.Client;
using WeDonekRpc.Helper.IdGenerator;
using WeDonekRpc.SqlSugar;

namespace Wedonek.Demo.Service.Service
{
    internal class DBOrderService : IDBOrderService
    {
        private readonly IRepository<DB.OrderDB> _Repository;

        public DBOrderService (IRepository<OrderDB> repository)
        {
            this._Repository = repository;
        }

        public long AddOrder (OrderAddModel order)
        {
            OrderDB add = order.ConvertMap<OrderAddModel, OrderDB>();
            add.Id = IdentityHelper.CreateId();
            add.AddTime = DateTime.Now;
            this._Repository.Insert(add);
            return add.Id;
        }
    }
}
